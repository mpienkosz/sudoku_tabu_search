package pl.edu.pw.elka.stud.isi.sudoku

import Array._
import scala.collection.mutable._


/*Maciek:
 *
 * Model klasy Sudoku, sprawdzony że działa dobrze
 *
 *
 * Input: board wypełniony 0 tam, gdzie są puste pola na planszy sudoku i
 * w niektórych polach wartościami podanymi przez użytkownika
 * Zakładamy, że dane podawane są rzędami
 *
 */


class Sudoku(val firstSwapped: Int, val secondSwapped: Int) {
  // Reprezentacje planszy w postaci jednowymiarowej, po kolei rzędami
  var board: Array[Int] = null
  // Liczba konfliktów w danym rzędzie
  var rowScore = new Array[Int](9)
  // Liczba konfliktów w danej kolumnie
  var colScore = new Array[Int](9)
  // wektor wszystkich cyferek
  var values = range(1, 10)
  // lista wszytkich klatek (małych kwadratów), w każdym przechowywane indeksy pól z boarda, które należą do danej klatki
  var grids: Array[List[Int]] = new Array[List[Int]](9)


  def initialize(data: Array[Array[Int]]) {
    board = data.flatten
    /*Tworzenie pierwszego rozwiązania - uzupełnianie planszy numerkami tak, by w małych kwadratach
    * nie było konfliktów - wtedy przy generowaniu ruchów będziemy tylko generowali zamiany w obrębie
    * jednej klatki, co ograniczy nam liczbę generowanych możliwych ruchów - Bardzo sprytnie! )
    * (ale sam tego nie wymyśliłem, przeczytałem w artykule:) ) . Dodatkowo dziąki temu, jeśli od razu po
    * wykonaniu ruchu przeliczymy wartości rowScore i colScore dla kolumn i wierszy z których były zamiany,
    * będziemy mieć wszystkie dane do funkcji oceny na talerzu i bardzo szybko będziemy mogli uzyskać jej wartość
    */
    for (i <- 0 to 2) {
      for (j <- 0 to 2) {
        grids(3 * i + j) = (range(0, 81).filter(x => {
          val offset = j * 9 * 3 + i * 3
          val res = (x >= offset && x < offset + 3) || (x >= offset + 9 && x < offset + 12) || (x >= offset + 18 && x < offset + 21)
          res
        })).toList
        val gridValues = grids(3 * i + j).filter(x => board(x) != 0)
        grids(3 * i + j) = grids(3 * i + j).filter(x => board(x) == 0)
        val vals = values.diff(gridValues.map(gr => board(gr)))
        for ((grid, grVal) <- grids(3 * i + j).zip(vals)) board(grid) = grVal
      }
    }
    // obliczanie score'ów dla wszystkich kolumn i wierszy, bo to pierwszy raz
    for (row <- 0 to 8) countRowScore(row)
    for (col <- 0 to 8) countColScore(col)
  }

  override def clone: Sudoku = {
    //czy się przekopiują tylko wartości?
    val s = new Sudoku(firstSwapped, secondSwapped)
    s.board = board.clone
    s.rowScore = rowScore.clone
    s.colScore = colScore.clone
    //nigdy nie modyfikujemy grids, więc nie trzeba klonować
    s.grids = grids
    s
  }

  //funkcja, w której wykonywany jest ruch
  def swap(first: Int, second: Int) {
    var tmp: Int = 0
    val col1 = first % 9
    val row1 = first / 9
    val col2 = second % 9
    val row2 = second / 9
    tmp = board(first)
    board(first) = board(second)
    board(second) = tmp
    countRowScore(row1)
    if (row1 != row2) countRowScore(row2)
    countColScore(col1)
    if (col1 != col2) countColScore(col2)
  }

  def countRowScore(row: Int) {
    val r = range(row * 9, (row + 1) * 9)
    rowScore(row) = values.diff(r.map(x => board(x))).size
  }

  def countColScore(col: Int) {
    val r = range(col, 81, 9)
    colScore(col) = values.diff(r.map(x => board(x))).size
  }

  //funkcja celu
  def goalFunction: Int = -rowScore.sum - colScore.sum

  def getMoves: Iterator[Sudoku => Sudoku] = {
    val res: ListBuffer[Sudoku => Sudoku] = new ListBuffer[Sudoku => Sudoku]()
    //użyć combinations ??
    for (grid <- grids)
      for (i <- 0 to grid.size - 1)
        for (j <- i + 1 to grid.size - 1)
          res.+=(new SingleMove(grid(i), grid(j)))
    res.toIterator
  }

  override def toString = {
    var res = ("-------------------------\n")
    for (r <- 0 to 8) {
      res += "| "
      for (c <- 0 to 8) {
        res += (board(r * 9 + c))
        if (r * 9 + c == firstSwapped || r * 9 + c == secondSwapped) res += (")")
        else res += (" ")
        if (c == 2 || c == 5) res += ("| ")
        if (c == 8) res += ("|\n")
      }
      if (r == 2 || r == 5 || r == 8) res += ("-------------------------\n")
    }
    res
  }

  def toArray = board.sliding(9, 9).toArray
}

case class SingleMove(first: Int, second: Int) extends ((Sudoku) => Sudoku) {
  override def apply(old: Sudoku) = {
    val newSudoku = new Sudoku(first, second)
    newSudoku.board = old.board.clone
    newSudoku.rowScore = old.rowScore.clone
    newSudoku.colScore = old.colScore.clone
    //nigdy nie modyfikujemy grids, więc nie trzeba klonować
    newSudoku.grids = old.grids
    newSudoku.swap(first, second)
    newSudoku
  }
}
